#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 28 10:49:10 2018

@author: lserpette

# Détermination des indices de grille CROCO correspondant à des points dont les
# coordonnées géographiques sont connues.
# en entrée:
# file_grid = le fichier bathy de la grille CROCO
# npts = nbr de points concernés
# lon_pts et lat_ps = longitudes et latitudes des points concernés.

"""

import netCDF4
import sys
import numpy as np
import xarray as xr 

# Read data
#file_grid="/misc/WORK/CROCO/PREPA_CROCO/GRID/GDMoZ/croco_grd_GDMoZ_roms_0.9.nc"
file_grid="/misc/WORK/CROCO/PREPA_CROCO/GRID/GROIX_10M/croco_grd_GROIX_10M_roms_0.99.nc"
#file_grid="/misc/WORK/CROCO/PREPA_CROCO/GRID/BZH_HYCOM/croco_grd_ucla.nc"

#read croco grid
ds = xr.open_dataset(file_grid)
lat_rho = ds['lat_rho']
lon_rho = ds['lon_rho']

#coordonnees des points modele 
#npts = 8
#lon_pts = [-2.922,-2.883938,-2.893222,-2.881285,-2.842821,-2.8468,-2.865369,-2.818946]
#lat_pts = [47.552,47.57437,47.57167,47.56987,47.55188,47.56538,47.58516,47.58966]
#npts = 4
#latz1 = 47.63333333
#latz2 = 47.65833333
#lonz1 = -3.45
#lonz2 = -3.40
#lon_pts = [lonz1,lonz1,lonz2,lonz2]
#lat_pts = [latz1,latz2,latz1,latz2]
#lon_pts = [-3.408016667,-3.36885,-3.445233333,-3.445852,-3.425983333]
#lat_pts = [47.679,47.708166667,47.661766667,47.644274,47.64715]
npts = 1
lon_pts = [-3.425033333]
lat_pts = [47.6091]

indlat_pts = np.zeros(npts)
indlon_pts = np.zeros(npts)

# calcul de distance
for i in range(0,npts):
    dist = (lon_pts[i] - lon_rho[:])**2 + (lat_pts[i] - lat_rho[:])**2
    indlat_pts[i] = np.where(dist.data == dist.data.min())[0][0]
    indlon_pts[i] = np.where(dist.data == dist.data.min())[1][0]
    
    print('indlat_pts['+str(i+1)+']=',indlat_pts[i], 'indlon_pts['+str(i+1)+']=',indlon_pts[i])













