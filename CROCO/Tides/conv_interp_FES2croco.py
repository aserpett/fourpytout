#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 12:05:42 2019

@author: Alain Serpette after Matthieu Caillaud
conversion des fichiers de forçage de maree (constantes harmoniques)
au format CROCO pour utilisation directe dans le modele
"""
import warnings
import netCDF4
import numpy as N
import cdms2

warnings.filterwarnings("ignore", category=FutureWarning)

def ap2ep(Au, PHIu, Av, PHIv):
  # Assume the input phase lags are in degrees and convert them in radians.
   PHIu = PHIu/180*N.pi
   PHIv = PHIv/180*N.pi

  # Make complex amplitudes for u and v
   u = Au*N.exp(-1j*PHIu)
   v = Av*N.exp(-1j*PHIv)

 # Calculate complex radius of anticlockwise and clockwise circles:
   wp = (u+1j*v)/2      # for anticlockwise circles
   wm = N.conj(u-1j*v)/2  # for clockwise circles

 # and their amplitudes and angles
   Wp = N.abs(wp)
   Wm = N.abs(wm)
   THETAp = N.angle(wp)
   THETAm = N.angle(wm)

 # calculate e-parameters (ellipse parameters)
   SEMA = Wp+Wm              # Semi  Major Axis, or maximum speed
   SEMI = Wp-Wm              # Semi  Minor Axis, or minimum speed
   ECC = SEMI/SEMA          # Eccentricity

   PHA = (THETAm-THETAp)/2   # Phase angle, the time (in angle) when 
                             # the velocity reaches the maximum
   INC = (THETAm+THETAp)/2   # Inclination, the angle between the 
                              # semi major axis and x-axis (or u-aconvert_fes2croco.pyxis).

 # convert to degrees for output
   PHA = PHA/N.pi*180
   INC = INC/N.pi*180
   THETAp = THETAp/N.pi*180
   THETAm = THETAm/N.pi*180

 # flip THETAp and THETAm, PHA, and INC in the range of 
 # [-pi, 0) to [pi, 2*pi), which at least is my convention.
   id = THETAp < 0;   THETAp[id] = THETAp[id]+360
   id = THETAm < 0;   THETAm[id] = THETAm[id]+360
   id = PHA < 0;      PHA[id] = PHA[id]+360
   id = INC < 0;      INC[id] = INC[id]+360

   return SEMA, ECC, INC, PHA

#%% declarations a faire par l'utilisateur
#rac of fes repository
rac='/home6/datawork/aserpett/PREPA_CROCO/TIDES/CSTES_FES2014/'
suff = '_FES2014-180_MED.nc'
file_grid ='/home1/datawork/jbrousta/TRAINING_2021/CONFIGS/GIBRALTAR_FINAL/INPUT/croco_GIB100_BR4_N40_grd.nc '
file_out = 'croco_15W_tides_FES2014_MED.nc'

#flag pour incorporation des vitesses aux frontieres
l_uv = True

#tab of tides to interpolate
tides = ['M2','N2','S2','K2','K1','O1','P1','Q1','2N2','M4','MN4','MS4','N4','S1','S4']

#%% fin des declarations a faire par l'utilisateur

#read tides and periods
# !!! le fichier tides.txt doit etre dans le repertoire utilisateur !!!
tides_param=N.loadtxt("tides.txt",skiprows=3,comments='^',usecols=(0,4),dtype=str)
tides_names=tides_param[:,0].tolist()
tides_periods=tides_param[:,1].tolist()
tides_names=[x.lower() for x in tides_names]
tides_names=N.array(tides_names)

f=cdms2.open(file_grid)
lon_rho=f('lon_rho')
lat_rho=f('lat_rho')
mask=f('mask_rho')
f.close()

nb_tides=len(tides)
print "there are %i tide constituents "%nb_tides

for i,tide in enumerate(tides):
    index=N.argwhere(tides_names==tide.lower())
    if (len(index)>0):
        print "tides %s is in the list"%(tide)
# get period
        period=N.float(tides_periods[index[0,0]])
        print "Period of the wave %s is %f"%(tide,period)  
# get tide amp & phase for elevation
        tide_file=rac + tides[i] + suff
        f = cdms2.open(tide_file)
        ni = f['ni'][:]
        nj = f['nj'][:]
        Ha  = f['Ha'][:]
        Hg  = f['Hg'][:]
        Ha[mask==0] = 0. 
        Hg[mask==0] = 0.
        if l_uv:
            Ua = f['Ua'][:]
            Ug = f['Ug'][:]
            Va = f['Va'][:]
            Vg = f['Vg'][:]
            Ua[mask==0]=0.
            Ug[mask==0]=0.
            Va[mask==0]=0.
            Vg[mask==0]=0.
            major,eccentricity,inclination,phase=ap2ep(Ua,Ug,Va,Vg)
        f.close()
        
#write out nc file
        if 'ncout' not in globals():
            #nj,ni=lon_rho.shape
            xi_rho,eta_rho=ni,nj
            ncout=netCDF4.Dataset(file_out,'w',format='NETCDF4_CLASSIC')
            ncout.createDimension('xi_rho', len(xi_rho))
            ncout.createDimension('eta_rho', len(eta_rho))
            ncout.createDimension('tide_period', nb_tides)
         #   print "tide period = %f" %tide_period
        
            ncout.createVariable('tide_Eamp','d',('tide_period','eta_rho','xi_rho',))
            ncout.createVariable('tide_Ephase','d',('tide_period','eta_rho','xi_rho',))
            ncout.createVariable('tide_period','d',('tide_period',))
            if l_uv:
                ncout.createVariable('tide_Cmin','d',('tide_period','eta_rho','xi_rho',))
                ncout.createVariable('tide_Cmax','d',('tide_period','eta_rho','xi_rho',))
                ncout.createVariable('tide_Cangle','d',('tide_period','eta_rho','xi_rho',))
                ncout.createVariable('tide_Cphase','d',('tide_period','eta_rho','xi_rho',))            

        ncout.variables['tide_period'][i]=period
        ncout.variables['tide_Eamp'][i,:,:]=Ha.data[:]
        ncout.variables['tide_Ephase'][i,:,:]=Hg.data[:]
        if l_uv:
            ncout.variables['tide_Cmin'][i,:,:]=major.data[:]*eccentricity.data[:]
            ncout.variables['tide_Cmax'][i,:,:]=major.data[:]
            ncout.variables['tide_Cangle'][i,:,:]=inclination[:,:]
            ncout.variables['tide_Cphase'][i,:,:]=phase[:,:]
            
    else:
        print "tides %s is not in the list! Check the name of the tide!"%(tide)
ncout.close()