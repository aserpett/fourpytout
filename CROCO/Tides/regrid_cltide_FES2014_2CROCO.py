#
# -*- coding: utf-8 -*-
# AUTEUR : Alain Serpette
# DATE CREATION : 2021 11
# Pour Chaine de generation de forcages maree :
# ENTREE : Constantes harmoniques en amplitudes, phases et courants
#          sur une grille FES2014
# SORTIE : Constantes harmoniques en amplitudes, phases et courants
#          regrides sur la grille cible (ici Croco) pour les ondes choisies.
# Ajout avril 2022:
# rotation des courants pour une grille orientée.    

#%% declarations
import os.path
import warnings
import numpy as np
import xarray as xr
import xesmf as xe
from erode_mask import erode_mask

warnings.filterwarnings("ignore", category=FutureWarning)

#%% declarations a faire par l'utilisateur
# !!! le script erode_mask (author S. Raynaud) doit etre dans le repertoire utilisateur !!!
zone = 'MED'
file_grid ='/misc/WORK/CROCO/PREPA_CROCO/GRID/TEST_MED/croco_GIB100_BR4_N40_grd.nc'
file_regrid = 'bilinear_MED.nc'
path_FES2014 = '/misc/WORK/CROCO/PREPA_CROCO/TIDES/PARAM/FES2014/'
suff_FES2014 = '.MED_FES2014cut-180.nc'
path_outfiles = '/misc/WORK/CROCO/PREPA_CROCO/TIDES/CSTES_FES2014/'
suff_outfiles = '_FES2014-180_'+zone+'.nc'

#choix des ondes
#tide = ['M2']
#tide = ['M2','N2','S2','K2','Nu2','K1','O1','P1','Q1','L2','2N2','Mu2','La2']
tide = ['M2','N2','S2','K2','K1','O1','P1','Q1','2N2','M4','MN4','MS4','N4','S1','S4']

niter = 2   #nb iterations pour eroder le masque cote
courants = True #True si on veut les courants
rotation = False #True si la grille est orientée

#%% fin des declarations de l'utilisateur

#%% Lecture grille tide[0] pour construction grid_in
tidefile = path_FES2014+tide[0]+suff_FES2014
f = xr.open_dataset(tidefile)
loni = f.longitude
lati = f.latitude
f.close()

#%% definition de la grille d'entree 
grid_in = xr.Dataset(coords={'lat':lati,'lon':loni})

#%% Lecture grille croco
fcroco = xr.open_dataset(file_grid)
lono=fcroco.lon_rho
lato=fcroco.lat_rho
depth = fcroco.h
fcroco.close()

#%% definition de la grille de sortie
grid_out = xr.Dataset(coords={'lat':lato,'lon':lono})
grid_out = grid_out.rename_dims(eta_rho='nj',xi_rho='ni')
grid_out = grid_out.assign_coords(ni=xr.DataArray(np.arange(lono.shape[1]),dims='ni'))
grid_out = grid_out.assign_coords(nj=xr.DataArray(np.arange(lono.shape[0]),dims='nj'))

#%% calcul des parametres du gridder
nwave = 0   #initialisation
if(nwave == 0 and not(os.path.isfile(file_regrid))):
    regridder = xe.Regridder(grid_in, grid_out, 'bilinear',filename=file_regrid, reuse_weights=False)
else:
    regridder = xe.Regridder(grid_in, grid_out, 'bilinear',filename=file_regrid, reuse_weights=True)

if rotation:
    rotate = 45.   #en degres

for i in range(0,len(tide)):
    tidefile = path_FES2014+tide[i]+suff_FES2014
    file_res = path_outfiles+tide[i]+suff_outfiles

    #%% Read tide FES2014
    f = xr.open_dataset(tidefile)
    ha = f.Ha
    hg = f.Hg
    if (courants):
        ua = f.Ua
        ug = f.Ug
        va = f.Va
        vg = f.Vg
    f.close()

    ha = xr.where(ha==1.e+10,np.nan,ha)
    hg = xr.where(hg>9.e+09,np.nan,hg)
    if (courants):
        ua = xr.where(ua==1.e+10,np.nan,ua)
        ug = xr.where(ug==1.e+10,np.nan,ug)
        va = xr.where(va==1.e+10,np.nan,va)
        vg = xr.where(vg==1.e+10,np.nan,vg)

    #%% application de erode_mask pour repousser la terre
    ha=erode_mask(ha,niter=niter)
    #erosion du masque des autres variables
    hg=erode_mask(hg,niter=niter)
    if (courants):
        ua=erode_mask(ua,niter=niter)
        ug=erode_mask(ug,niter=niter)
        va=erode_mask(va,niter=niter)
        vg=erode_mask(vg,niter=niter)

    #%% Passage en coordonnees polaires avec rotation pour les courants
    hx = ha.copy()
    hy = ha.copy()
    hx[:] *= np.cos(np.radians(hg))
    hy[:] *= np.sin(np.radians(hg))

    if (courants):
        ux = ua.copy()
        uy = ua.copy()
        vx = va.copy()
        vy = va.copy()
        if rotation:
            ux[:] *= np.cos(np.radians(ug-rotate))
            uy[:] *= np.sin(np.radians(ug-rotate))
            vx[:] *= np.cos(np.radians(vg-rotate))
            vy[:] *= np.sin(np.radians(vg-rotate))            
        else:
            ux[:] *= np.cos(np.radians(ug))
            uy[:] *= np.sin(np.radians(ug))           
            vx[:] *= np.cos(np.radians(vg))
            vy[:] *= np.sin(np.radians(vg))

    # Passage des DataArray en Dataset avec changement des noms de variables
    if (courants):
        pol_in = xr.Dataset({'Hx':hx, 'Hy':hy, 'Ux':ux, 'Uy':uy, 'Vx':vx, 'Vy':vy})
    else:
        pol_in = xr.Dataset({'Hx':hx, 'Hy':hy})

    #%% application du gridder
    pol_out = regridder(pol_in)

    #%% reconversion depuis les polaires
    har = np.sqrt(pol_out.Hx**2 + pol_out.Hy**2)
    hgr = np.degrees(np.arctan2(pol_out.Hy, pol_out.Hx))
    
    if (courants):
        uar = np.sqrt(pol_out.Ux**2 + pol_out.Uy**2)
        ugr = np.degrees(np.arctan2(pol_out.Uy, pol_out.Ux))

        var = np.sqrt(pol_out.Vx**2 + pol_out.Vy**2)
        vgr = np.degrees(np.arctan2(pol_out.Vy, pol_out.Vx))
        
    # Passage des DataArray en Dataset avec changement des noms de variables
    if (courants):
        res_out = xr.Dataset({'Ha':har, 'Hg':hgr, 'Ua':uar, 'Ug':ugr, 'Va':var, 'Vg':vgr})
    else:
        res_out = xr.Dataset({'Ha':har, 'Hg':hgr})
       
    #%% ecriture
    if (courants):
        encoding = {'lat': {'_FillValue': False},
                    'lon': {'_FillValue': False},
                    'Ha': {'_FillValue': 1.e+10,'missing_value': 1.e+10},
                    'Hg': {'_FillValue': 1.e+10,'missing_value': 1.e+10},
                    'Ua': {'_FillValue': 1.e+10,'missing_value': 1.e+10},
                    'Ug': {'_FillValue': 1.e+10,'missing_value': 1.e+10},
                    'Va': {'_FillValue': 1.e+10,'missing_value': 1.e+10},            
                    'Vg': {'_FillValue': 1.e+10,'missing_value': 1.e+10}
                    }
    else:
        encoding = {'lat': {'_FillValue': False},
                    'lon': {'_FillValue': False},
                    'Ha': {'_FillValue': 1.e+10,'missing_value': 1.e+10},
                    'Hg': {'_FillValue': 1.e+10,'missing_value': 1.e+10}             
                    }

    #Ecriture dans un fichier netcdf qui pourra etre reutilise pour conversion
    #en un fichier de forcage pour croco avec toutes les ondes necessaires
    res_out.to_netcdf(file_res, encoding=encoding, engine = 'netcdf4')
    nwave = nwave+1
