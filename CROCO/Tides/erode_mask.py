#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 15:25:14 2020

@author: lsraynaud
"""
import numpy as np
import scipy.signal as ss
import xarray as xr

def np_filternd(data, kernel):
    if np.isscalar(kernel):
        kernel = np.ones((kernel,)*data.ndim)
    else:
        assert data.ndim == kernel.ndim

    # Guess mask
    bad = np.isnan(data)
    with_mask = bad.any()
    if with_mask:
        data = np.where(bad, 0, data)

    # Convolutions
    cdata = ss.convolve(data, kernel, mode='same')
    weights = ss.convolve((~bad).astype('i'), kernel, mode='same')
    weights = np.clip(weights, 0, kernel.sum())

    # Weigthing and masking
    if with_mask:
        bad = np.isclose(weights, 0)
        weights[bad] = 1
    cdata /= weights
    if with_mask:
        cdata[bad] = np.nan
    return cdata

def xr_filternd(data, kernel):
    if isinstance(kernel, xr.DataArray) and  kernel.dims != data.dims:
        kernel = kernel.transpose(data.dims)
    datac = np_filternd(data, kernel)
    datac = xr.DataArray(datac, coords=data.coords, attrs=data.attrs)
    return datac


def erode_mask(data, niter, kernel=3):
    for i in range(niter):
        datac = xr_filternd(data, kernel)
        data = xr.where(np.isnan(data), datac, data)
    return data


if __name__ == '__main__':

    ds = xr.open_dataset("/home/shom/serpette/pol_out.nc")

    hx = erode_mask(ds.Hx, 1)

    bb = np_filternd(ds.Hx.data, 3)
    print(np.nanmin(bb), np.nanmax(bb))


    hx.plot.contourf('lon', 'lat')
